package powershellconsole;

import com.davidak.dev.powershellconsole.PowershellConsole;
import com.davidak.dev.powershellconsole.exceptions.PowershellException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Operations for executing Windows Powershell Scripts from Java.
 *
 * @author David Janicek (davidak)
 */
public abstract class PowershellConsoleTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(PowershellConsoleTest.class);

	private static PowershellConsole powershellConsole;

	public static void main(String[] args) {
		// start console, will be running until close() method is called
		powershellConsole = PowershellConsole.getPowershellConsole();

		// invoke command from string
		powershellConsole.invokeExpression("Get-Date", "Get-Date");

		// invoke command from file
		executePowershellScriptFromFile("Get-Date", "Get-Date");

		// close console
		powershellConsole.close();
	}


	public static void executePowershellScriptFromFile(String code, String scriptFileContent) {
		if (StringUtils.isEmpty(scriptFileContent)) {
			LOGGER.warn("Command for Powershell script {} is empty, cannot process command.", code);
			return;
		}

		try {
			Path scriptPath = Files.write(Files.createTempFile(code, ".ps1"), scriptFileContent.getBytes(Charset.forName("windows-1250")));

			if (!Files.exists(scriptPath)) {
				throw new PowershellException("File with Powershell script does not exist: " + scriptPath.toString());
			}

			if (powershellConsole != null && !powershellConsole.isClosed()) {
				powershellConsole.invokeScriptFile(scriptPath, code);
			} else {
				throw new PowershellException("Powershell konzole neexistuje nebo je již ukončena.");
			}
		} catch (IOException ex) {
			throw new PowershellException("Při spuštění Powershell skriptu došlo k chybě.", ex);
		}
	}


}