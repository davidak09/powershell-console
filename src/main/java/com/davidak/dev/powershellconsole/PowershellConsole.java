package com.davidak.dev.powershellconsole;


import com.davidak.dev.powershellconsole.utils.PowershellStreamGobbler;
import com.davidak.dev.powershellconsole.utils.PowershellUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Main class for executing Powershell scripts.
 *
 * @author David Janicek (davidak)
 */
public class PowershellConsole {

	private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

	private static final Logger LOGGER = LoggerFactory.getLogger(PowershellConsole.class);

	private Process process;

	private PrintWriter writer;

	private boolean closed;

	private long startTime;

	private PowershellConsole(Process process) {
		LOGGER.debug("Starting Powershell Console with process {}.", process);
		if (process != null) {
			closed = false;
			this.process = process;

			// standard system output
			PowershellStreamGobbler outputGobbler = new PowershellStreamGobbler(Charset.forName("CP852"), null);
			outputGobbler.setInputStream(process.getInputStream());

			// error system output
			PowershellStreamGobbler errorGobbler = new PowershellStreamGobbler(Charset.forName("CP852"), null);
			errorGobbler.setInputStream(process.getErrorStream());

			writer = new PrintWriter(new OutputStreamWriter(new BufferedOutputStream(process.getOutputStream())), true);

			// kick them off
			outputGobbler.start();
			errorGobbler.start();

			startTime = System.currentTimeMillis();
		} else {
			closed = true;
		}
	}

	public static PowershellConsole getPowershellConsole() {
		String command = "powershell";

		List<String> commandArgs = new ArrayList<>();
		commandArgs.add(command);
		commandArgs.add("-ExecutionPolicy");
		commandArgs.add("UnRestricted");
		commandArgs.add("-NoLogo");
		commandArgs.add("-NoProfile");
		commandArgs.add("-NonInteractive");
		commandArgs.add("-NoExit");
		commandArgs.add("-Command");
		commandArgs.add("-");

		ProcessBuilder pb = new ProcessBuilder();
		pb.command(commandArgs);

		LOGGER.debug("Starting Powershell command with parameters {}.", commandArgs);

		Process process;
		try {
			process = pb.start();
			LOGGER.debug("Powershell process {} has been started.", process);
		} catch (IOException e) {
			LOGGER.error("Error while starting Powershell process.", e);
			process = null;
		}

		return new PowershellConsole(process);
	}

	public void execute(String command, String code) {
		if (!isClosed()) {
			LOGGER.debug("Executing command {} in Powershell Console.", command);
			writer.println("Write-Host \"-----------------------------------------------------------------------------------------\"");
			writer.println("Write-Host \"" + LocalDateTime.now().format(DATE_TIME_FORMATTER) + " - command " + code + " execution start.\"");
			writer.println(command);
			writer.println("Write-Host \"" + LocalDateTime.now().format(DATE_TIME_FORMATTER) + " - command " + code + " execution end.\"");
			writer.println("Write-Host \"-----------------------------------------------------------------------------------------\"");
			writer.flush();
		} else {
			LOGGER.warn("Powershell Console has been already closed.");
		}
	}

	public void invokeScriptFile(Path powershellScriptPath, String code) {
		execute("Invoke-Expression -Command \"" + powershellScriptPath.toAbsolutePath().toString() + "\"", code);
	}

	public void invokeExpression(String scriptBlock, String code) {
		execute("Invoke-Command -ScriptBlock {" + PowershellUtils.getScriptBlock(scriptBlock) + "}", code);
	}

	public boolean isClosed() {
		return closed;
	}

	public int close() {
		int exitValue = -1;
		if (!isClosed()) {
			execute("exit", "exit");

			try {
				boolean finished = process.waitFor(5L, TimeUnit.SECONDS);
				LOGGER.debug("Process {} is finished: {}", process, finished);

				if (finished) {
					exitValue = process.exitValue();
					LOGGER.info("Process exited with return code {}.", exitValue);
				}

			} catch (InterruptedException e) {
				LOGGER.error("Process was interrupted.", e);
				exitValue = -1;

			} finally {
				if (process.isAlive()) {
					process.destroyForcibly();
					LOGGER.error("Process was forcibly destroyed after specified time.");
				}
				long stopTime = System.currentTimeMillis();
				long elapsedTime = stopTime - startTime;

				closed = true;
				LOGGER.info("Process {} was running for {} ms.", process, elapsedTime);
			}
		} else {
			LOGGER.info("Powershell Console has been already closed.");
		}
		return exitValue;
	}
}