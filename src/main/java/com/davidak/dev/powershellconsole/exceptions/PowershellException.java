package com.davidak.dev.powershellconsole.exceptions;

/**
 * Powershell Exceptions wrapper.
 *
 * @author David Janicek (davidak)
 */
public class PowershellException extends RuntimeException {

	private static final long serialVersionUID = 5915945470230041388L;

	public PowershellException(String msg) {
		super(msg);
	}

	public PowershellException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
