package com.davidak.dev.powershellconsole.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * Stream gobbler for wrapping Powershell logging events.
 *
 * @author David Janicek (davidak)
 */
public class PowershellStreamGobbler extends StreamGobbler {

	protected final Logger LOGGER = LoggerFactory.getLogger(PowershellStreamGobbler.class);

	public PowershellStreamGobbler(InputStream is, Charset charset, String type) {
		super(is, charset, type);
	}

	public PowershellStreamGobbler(Charset charset, String type) {
		super(charset, type);
	}
}