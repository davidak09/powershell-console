package com.davidak.dev.powershellconsole.utils;

/**
 * Utils for processing Powershell scripts.
 *
 * @author David Janicek (davidak)
 */
public class PowershellUtils {

	public static String getScriptBlock(String script) {
		// delete all comments and new lines
		return script.replaceAll("(?m)^\\s*#.*$", "").replaceAll("\n|\r", "");
	}
}