package com.davidak.dev.powershellconsole.utils;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

/**
 * Wrapper for logging Powershell events from operating system terminal.
 *
 * @author David Janicek (davidak)
 */
public class StreamGobbler extends Thread {

	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private InputStream inputStream;

	private Charset charset;

	private String type;

	public StreamGobbler(InputStream inputStream, Charset charset, String type) {
		this.inputStream = inputStream;
		this.charset = charset;
		this.type = type;
	}

	public StreamGobbler(Charset charset, String type) {
		this.charset = charset;
		this.type = type;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public Charset getCharset() {
		return charset;
	}

	public void setCharset(Charset charset) {
		this.charset = charset;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void run() {
		try {
			InputStreamReader isr = new InputStreamReader(inputStream, charset);
			BufferedReader br = new BufferedReader(isr);
			String line;

			if (StringUtils.isNoneEmpty(type)) {
				while ((line = br.readLine()) != null) {
					LOGGER.debug("{}: {}", type, line);
				}
			} else {
				while ((line = br.readLine()) != null) {
					LOGGER.debug("{}", line);
				}
			}
		} catch (IOException ioe) {
			LOGGER.error("Error occurred while reading {} stream. Error: {}", type, ioe);
		}
	}
}