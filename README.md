# Powershell Console

Powershell Console is an utility library for comfortable running of Powershell commands and scripts within a Windows environment.

It starts so called console, wrapper for Windows command line, where user can instantly invoke commands. It runs until user calls close() method,
so it can speed up execution of multiple following commands because it doesn't have to start Powershell environment for each call separately.

It's a Maven project a has only few dependencies:

* ch.qos.logback:logback
* org.apache.commons:commons-lang3
* also requires Java 8 environment for running

Sample usage is shown in test class PowershellConsoleTest.java. It can accept single commands as String or Path to script file.

```java
public static void main(String[] args) {
	// start console, will be running until close() method is called
	PowershellConsole powershellConsole = PowershellConsole.getPowershellConsole();

	// invoke command from string
	powershellConsole.invokeExpression("Get-Date", "Get-Date");

	// close console
	powershellConsole.close();
}
```

Here is an example usage of Powershell Console in Spring Framework, where Powershell Console is handled as an Java Bean.


```java
@Bean(destroyMethod = "close")
@Lazy
public PowershellConsole powershellConsole() {
	PowershellConsole powershellConsole = PowershellConsole.getPowershellConsole();
	// needed for executing not-signed scripts
	powershellConsole.execute("Set-ExecutionPolicy UnRestricted", "Set-ExecutionPolicy");
	return powershellConsole;
}
```